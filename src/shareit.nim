import std/asyncdispatch
import std/asynchttpserver
import std/json
import std/logging
import std/os
import std/strformat
import std/tables
import std/uri
import strutils
import sugar


const
  frontBuildDir = "front/build"


### Config

type
  Config = object
    allowTokens: seq[string]

proc readConfig(path: string): Config =
  path.readFile.parseJson.to Config


### Data

type
  Text = object
    id: int
    text: string
  TextStore = ref object
    offset: int
    items: TableRef[int, string]

func newTextStore(): TextStore =
  TextStore(offset: 0, items: newTable[int, string]())

func add(s: TextStore, text: string): int =
  s.offset += 1
  result = s.offset
  s.items[s.offset] = text

func remove(s: TextStore, id: int) =
  s.items.del id

func list(s: TextStore): seq[Text] =
  for id, text in s.items.pairs():
    result.add Text(id: id, text: text)


### API

func getAuthHeader(req: Request): string =
  req.headers.getOrDefault "Authorization"

proc notFound(req: Request): auto =
  req.respond(Http404, "")

proc methodNotAllowed(req: Request): auto =
  req.respond(Http405, "")

proc jsonResp(req: Request, code: HttpCode, content: JsonNode,
              headers: HttpHeaders = nil): Future[void] =
  let h = if headers == nil: newHttpHeaders(true) else: headers
  h["Content-Type"] = "application/json"
  req.respond(code, $content, h)

proc parseJson(
  req: Request,
  h: (proc (req: Request, data: JsonNode) {.async.}),
): auto =
  var j: JsonNode
  try:
    j = req.body.parseJson
  except JsonParsingError:
    return req.jsonResp(Http400, %* {
      "msg": "Problems parsing JSON",
    })

  return h(req, j)

proc parsePathIdInt(
  req: Request,
  path: string,
  h: (proc (req: Request, id: int) {.async.}),
): auto =
  var path = path
  path.removePrefix "/"
  var id: int
  try:
    id = parseInt path
  except ValueError:
    return req.jsonResp(Http400, %* {
      "msg": "id should be int",
    })

  return h(req, id)

proc parsePathIdStr(
  req: Request,
  path: string,
  h: (proc (req: Request, id: string) {.async.}),
): auto =
  var id = path
  id.removePrefix "/"
  h(req, id.decodeUrl false)


### Service

type
  TextSvc = object
    stores: TableRef[string, TextStore]

func newTextSvc(): auto =
  TextSvc(stores: newTable[string, TextStore]())

func getStore(s: TextSvc, storeId: string): TextStore =
  if storeId notin s.stores:
    s.stores[storeId] = newTextStore()

  s.stores[storeId]

proc create(s: TextSvc, req: Request, storeId: string, j: JsonNode): auto =
  let text = j{"text"}.getStr
  if text == "":
    req.jsonResp(Http422, %* {
      "msg": "Blank text field",
    })
  else:
    let id = s.getStore(storeId).add text
    req.jsonResp(Http201, %* {
      "id": id,
    })

proc delete(s: TextSvc, req: Request, storeId: string, id: int): auto =
  if id == 0:
    req.jsonResp(Http422, %* {
      "msg": "Blank id field",
    })
  else:
    s.getStore(storeId).remove id
    req.respond(Http204, "")

proc list(s: TextSvc, req: Request, storeId: string): auto =
  let items = s.getStore(storeId).list
  req.jsonResp(Http200, %* {
    "count": items.len,
    "items": items,
  })


type
  TokenSvc = object
    allowTokens: seq[string]

func newTokenSvc(allowTokens: seq[string]): auto =
  TokenSvc(allowTokens: allowTokens)

proc get(s: TokenSvc, req: Request, id: string): auto =
  if id notin s.allowTokens:
    req.notFound
  else:
    req.jsonResp(Http200, %* {
      "token": id,
    })


proc handleFile(req: Request, filename: string): auto =
  var text: string
  try:
    text = readFile filename
  except IOError:
    return req.notFound

  let headers = newHttpHeaders(true)
  case filename.splitFile.ext
  of ".svg":
    headers["Content-Type"] = "image/svg+xml"

  req.respond(Http200, text, headers)

func tokenHandler(allowTokens: seq[string]): auto =
  let svc = newTokenSvc(allowTokens)

  result = proc(req: Request, path: string): auto =
    if path.startsWith "/":
      case req.reqMethod
      of HttpGet:
        req.parsePathIdStr(
          path, (req, id) => svc.get(req, id)
        )
      else:
        req.methodNotAllowed
    else:
      req.notFound

func textHandler(): auto =
  let svc = newTextSvc()

  result = proc(req: Request, path: string): auto =
    var path = path
    let authHeader = req.getAuthHeader
    case req.reqMethod:
      of HttpGet:
        svc.list(req, authHeader)
      of HttpPost:
        req.parseJson (req, j) => svc.create(req, authHeader, j)
      of HttpDelete:
        path.removePrefix "/texts"
        if path.startsWith "/":
          req.parsePathIdInt(
            path, (req, id) => svc.delete(req, authHeader, id)
          )
        else:
          req.methodNotAllowed
      else:
        req.methodNotAllowed

proc apiHandler(allowTokens: seq[string]): auto =
  let
    tokenHdl = tokenHandler(allowTokens)
    textHdl = textHandler()

  result = proc(req: Request, path: string): auto =
    var path = path
    let authHeader = req.getAuthHeader

    if path.startsWith "/tokens":
      path.removePrefix "/tokens"
      tokenHdl req, path
    elif authHeader == "":
      req.respond(Http401, "")
    elif authHeader notin allowTokens:
      req.respond(Http403, "")
    elif path.startsWith "/texts":
      textHdl req, path
    else:
      req.notFound

func newHandler(allowTokens: seq[string]): auto =
  let apiHdl = apiHandler(allowTokens)
  result = proc (req: Request): auto =
    let
      mthd = req.reqMethod
      hostname = req.hostname
    var path = req.url.path
    # TODO Add response fields
    info &"{hostname=}, {mthd=}, {path=}"

    if path == "/":
      req.handleFile frontBuildDir / "index.html"
    elif path.startsWith "/static/":
      req.handleFile frontBuildDir / path
    elif path.startsWith "/api":
      path.removePrefix "/api"
      apiHdl req, path
    else:
      req.handleFile(frontBuildDir / path)


### Server

proc main {.async.} =
  var consoleLogger = newConsoleLogger(fmtStr = verboseFmtStr)
  addHandler consoleLogger

  let conf = readConfig "config.json"
  var server = newAsyncHttpServer()
  server.listen(Port(8010))
  let port = server.getPort
  echo &"Server listening on port: {port.uint16}"
  let handler = newHandler(conf.allowTokens)
  while true:
    if server.shouldAcceptRequest():
      await server.acceptRequest(handler)
    else:
      # too many concurrent connections, `maxFDs` exceeded.
      # wait 500ms for FDs to be closed.
      await sleepAsync(500)


when isMainModule:
  waitFor main()
