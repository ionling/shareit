FROM nimlang/nim:1.6.0-alpine

WORKDIR app

COPY shareit.nimble .
COPY src ./src

RUN nimble build -y -d:release

COPY front/build ./front/build

CMD ./shareit
