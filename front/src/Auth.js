import axios from "axios";
import { useState } from "react";

export default function Auth(props) {
  const [token, setToken] = useState("");
  const { onSuccess } = props;

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (token === "") {
      alert("blank token");
      return;
    }

    try {
      await axios.get(`/tokens/${token}`);
    } catch (e) {
      if (e.response.status === 404) {
        alert("invalid token");
      }
      return;
    }

    setToken("");
    onSuccess && onSuccess(token);
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Token:
        <input
          type="text"
          value={token}
          onChange={(e) => setToken(e.target.value)}
        />
      </label>
      <input type="submit" value="submit" />
    </form>
  );
}
