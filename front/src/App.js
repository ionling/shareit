import axios from "axios";
import { useState } from "react";
import "./App.css";
import Auth from "./Auth";
import logo from "./logo.svg";
import TextPanel from "./Text";

const lsTokenKey = "token";
const lsTokenSep = "::";
const monthTs = 30 * 24 * 60 * 60 * 1000;

function getAuthToken() {
  const s = localStorage.getItem(lsTokenKey);
  if (s === null || s === "") {
    return "";
  }

  const [ts, token] = s.split(lsTokenSep);

  if (Date.now() - ts > monthTs) {
    localStorage.setItem(lsTokenKey, "");
    return "";
  }

  return token;
}

function setAuthToken(token) {
  const s = `${Date.now()}${lsTokenSep}${token}`;
  localStorage.setItem(lsTokenKey, s);
}

function clearAuthToken() {
  localStorage.removeItem(lsTokenKey);
}

axios.defaults.baseURL = "/api";

function setAxiosToken(token) {
  axios.defaults.headers.common["Authorization"] = token;
}

function App() {
  const [needAuth, setNeedAuth] = useState(false);

  setAxiosToken(getAuthToken());

  axios.interceptors.response.use(
    function (resp) {
      return resp;
    },
    function (error) {
      if ([401, 403].includes(error.response.status)) {
        setNeedAuth(true);
      }
      return Promise.reject(error);
    }
  );

  const handleSuccess = (token) => {
    setAuthToken(token);
    setAxiosToken(token);
    setNeedAuth(false);
  };

  const handleClearToken = () => {
    clearAuthToken();
    setNeedAuth(true);
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        ShareIt
      </header>
      {needAuth ? (
        <Auth onSuccess={handleSuccess} />
      ) : (
        <div>
          <div className="menu">
            <button onClick={handleClearToken}>Clear token</button>
          </div>
          <TextPanel />
        </div>
      )}
    </div>
  );
}

export default App;
