import axios from "axios";
import { useEffect, useState } from "react";
import "./Text.scss";

function Text(props) {
  const { item, onDelete } = props;
  const handleClick = () => {
    onDelete && onDelete(item.id);
  };

  return (
    <div className="text-item">
      <span className="delete-btn" onClick={handleClick}>
        x
      </span>
      <span>{item.text}</span>
    </div>
  );
}

function TextInput(props) {
  const [text, setText] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();

    const { onSubmit } = props;
    onSubmit && onSubmit(text);
    setText("");
  };

  return (
    <form className="text-input" onSubmit={handleSubmit}>
      <textarea value={text} onChange={(e) => setText(e.target.value)} />
      <input type="submit" value="create" />
    </form>
  );
}

export default function TextPanel() {
  const [texts, setTexts] = useState([]);

  const refreshTexts = async () => {
    const resp = await axios.get("texts");
    setTexts(resp.data.items);
  };

  const handleSubmit = async (text) => {
    await axios.post("/texts", { text });
    await refreshTexts();
  };

  const handleDelete = async (id) => {
    await axios.delete(`/texts/${id}`);
    await refreshTexts();
  };

  useEffect(() => {
    refreshTexts();
  }, []);

  return (
    <div className="text-panel">
      <TextInput onSubmit={handleSubmit} />
      <div className="text-items">
        {texts.map((x) => (
          <Text key={x.id} item={x} onDelete={handleDelete} />
        ))}
      </div>
    </div>
  );
}
