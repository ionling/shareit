# Package

version       = "0.1.2"
author        = "ionling"
description   = "Backend of shareit"
license       = "MIT"
srcDir        = "src"
bin           = @["shareit"]


# Dependencies

requires "nim >= 1.6.0"
