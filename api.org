# -*- org-confirm-babel-evaluate: nil -*-
#+PROPERTY: header-args :noweb yes :results output :wrap src json

* Variables
#+name: vars
#+begin_src sh
  local_api=":8010/api"
  texts_api="$local_api/texts"
  tokens_api="$local_api/tokens"

  # request
  r() {
      http --ignore-stdin --pretty=format $@ Authorization:1234
  }

  # raw request
  rr() {
      http --ignore-stdin --pretty=format $@
  }
#+end_src

* Error

** Parsing JSON
#+begin_src sh
  <<vars>>
  r POST "$texts_api"
#+end_src

#+RESULTS:
#+begin_src json
{
    "msg": "Problems parsing JSON"
}
#+end_src

** Blank field
#+begin_src sh
  <<vars>>
  r POST "$texts_api" id:=1234
#+end_src

#+RESULTS:
#+begin_src json
{
    "msg": "Blank text field"
}
#+end_src

* Texts

** Create

*** one
#+begin_src sh
  <<vars>>
  r POST "$texts_api" text=1234
#+end_src

#+RESULTS:
#+begin_src json
{
    "id": 4
}
#+end_src

*** two
#+begin_src sh
  <<vars>>
  rr POST "$texts_api" text=1234 Authorization:2234
#+end_src

#+RESULTS:
#+begin_src json
{
    "id": 1
}
#+end_src

** List
#+begin_src sh
  <<vars>>
  r GET "$texts_api"
#+end_src

#+RESULTS:
#+begin_src json
{
    "count": 3,
    "items": [
        {
            "id": 4,
            "text": "1234"
        },
        {
            "id": 3,
            "text": "1235"
        },
        {
            "id": 1,
            "text": "1236"
        }
    ]
}
#+end_src

** Delete
#+begin_src sh
  <<vars>>
  r DELETE "$texts_api/2"
#+end_src

#+RESULTS:
#+begin_src json
#+end_src

* Tokens
** Get
#+begin_src sh
  <<vars>>
  rr GET "$tokens_api/1234"
#+end_src

#+RESULTS:
#+begin_src json
{
    "token": "1234"
}
#+end_src

#+begin_src sh
  <<vars>>
  r -v GET "$tokens_api/8234" 2>&1 | grep 404
#+end_src

#+RESULTS:
#+begin_src json
HTTP/1.1 404 Not Found
#+end_src
